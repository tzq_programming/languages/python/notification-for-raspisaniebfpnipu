from datetime import date


class Notifier:
    # ==================================================================================================================
    # защищенные методы
    def _get_users_and_messages(self, service_name):
        """
        публичный метод для начала уведомлений
        :return:
        """
        return self.__get_users_and_messages(service_name)

    # ==================================================================================================================
    # приватные методы
    def __get_users_and_messages(self, service_name):
        """
        Приватный метод в котором происходит следующее:
            1) получить строки подписчиков из базы
            2) для каждой строки получить расписание
            3) преобразовать расписание
            4) отправить расписание
        :return:
        """
        user_option = self.__get_subscribes_list(service_name)
        timetable = self.__get_timetable_list(user_option)
        transformed_timetable = self.__transform_timetable(timetable)

        return transformed_timetable

    def __get_subscribes_list(self, service_name) -> dict:
        """
        Метод в котором происходит вызов метода для отправки запроса на Ядро 
        для получения списка подписчиков {user_id: option_id}
        :return: словарь {user_id: option_id}
        """
        params = {
            "service": "subscribers_list",
            "id": service_name
        }

        data = self.__send_request(params)
        return data["data"]

    def __get_timetable_list(self, user_option: dict) -> dict:
        """
        Метод в котором происходит получение расписания для каждого user_id
        :param user_option: словарь {user_id: option_id}
        :return: словарь {user_id: {'name': 'option_name', 'lessons': {}, 'tomorrow': "дата"} }
        """
        timetable_list = {}
        for user_id, option_id in user_option.items():
            tt_for_option = self.__get_timetable_for_option(option_id)
            # tt_for_option == {'status': 'ok', 'data': {'name': 'ПОВТ-16д', 'lessons': {}}}

            if tt_for_option.get("status") == "error":
                timetable_list[user_id] = None
            else:
                timetable_list[user_id] = tt_for_option.get("data")
                timetable_list[user_id]["tomorrow"] = tt_for_option.get("tomorrow")

        return timetable_list

    @staticmethod
    def __transform_timetable(timetable: dict) -> dict:
        """
        Метод в котором происходит преобразование словаря с пользовательскими расписанием в список сообщений для
        отправки пользователю user_id
        :param timetable: словарь пользовательскими расписанием
        :return: список сообщений
        """
        transformed_timetable = {}

        for user_id, lessons_data in timetable.items():
            name: str = lessons_data["name"]
            lessons: dict = lessons_data["lessons"]
            tomorrow: str = lessons_data["tomorrow"]
            t_or_g: str = "teach_name" if len(name) < 15 else "group_name"

            transformed_lessons = [TITLE_FORMAT % (tomorrow, name)]
            for lesson_date, day_lessons in lessons.items():
                for day_lesson in day_lessons:
                    transformed_lessons.append(
                        LESSONS_FORMAT % (
                        str(day_lesson["num"]),
                        day_lesson["begin"],
                        day_lesson["end"],
                        day_lesson["subject"],
                        day_lesson["type"],
                        day_lesson[t_or_g],
                        day_lesson["classroom"]
                        )
                    )

            transformed_timetable[user_id] = transformed_lessons

        return transformed_timetable

    # ==================================================================================================================
    # дополнительные методы
    def __get_timetable_for_option(self, option) -> dict:
        """
        Метод в котором формируется завтрашний день, 
        параметры запроса и происходит вызов отправки запроса на Ядро
        """
        # получение завтрашней даты
        timestamped_tomorrow = int(date.strftime(date.today(), "%s")) + 86400
        tomorrow = date.strftime(date.fromtimestamp(timestamped_tomorrow), '%d.%m.%Y')

        params = {
            "service": "option",
            "id": option,
            "date_begin": tomorrow,
            "date_end": tomorrow
        }

        response = self.__send_request(params)
        response["tomorrow"] = tomorrow

        return response

    @staticmethod
    def __send_request(data):
        """
        Метод в котором происходит отправка запроса с параметрами на Ядро
        """
        import requests
        import json

        encoded_data = json.dumps(data)
        response = requests.post("http://your-url/api/", data=encoded_data)
        response = json.loads(response.content)

        return response


# ======================================================================================================================
# форматы
TITLE_FORMAT = "‼‼‼Завтра(%s) у %s ‼‼‼"
LESSONS_FORMAT = "%s) %s-%s: «%s %s» у %s в %s ауд."
