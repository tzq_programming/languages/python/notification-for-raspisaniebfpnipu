import time
import schedule
from notifier_vk import VKNotifier
from notifier_telegram import TelegramNotifier
from tokens import vk_token, tg_token


def send_notification():
    """
    Инициализация классов и запуск уведомлений
    """
    vk = VKNotifier(vk_token)
    tg = TelegramNotifier(tg_token)

    vk.do_notify()
    tg.do_notify()

# отправка сообщений каждый день в 14:00 по МСК
while 1:
    schedule.every().day.at("14:00").do(send_notification)
    time.sleep(3600)
