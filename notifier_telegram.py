from notifier import Notifier


class TelegramNotifier(Notifier):
    def __init__(self, token):
        self.__token = token

    def do_notify(self):
        """
        Публичный Метод уведомления.
        Здесь происходит вызов приватного метода
        """
        self.__do_notify("telegram")

    def __do_notify(self, service_name):
        """
        Приватный метод уведомления
        1) Получаем словарь {user_id: [message_list]}
        2) передаём словарь на отправку
        """
        user_and_messages = self._get_users_and_messages(service_name)
        self.__send_message(user_and_messages)

    def __send_message(self, transformed_timetable: dict):
        """
        Метод в котором происходит отправка сообщений пользователям
        :param transformed_timetable: словарь {user_id: [список сообщений]}
        """
        import telebot

        bot = telebot.TeleBot(self.__token)

        for user_id, message_list in transformed_timetable.items():
            if len(message_list) < 2:
                continue

            for message in message_list:
                bot.send_message(user_id, message)
